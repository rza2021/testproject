from xdist.scheduler.loadscope import LoadScopeScheduling
from playwright.sync_api import Page
from slugify import slugify
import allure
import pytest
import requests


class MyScheduler(LoadScopeScheduling):
    def _split_scope(self, nodeid):
        if 'tests/non-parallel-run' in nodeid:
            return 'non-parallel-run-tests'
        return nodeid


def pytest_xdist_make_scheduler(config, log):
    return MyScheduler(config, log)


def pytest_runtest_makereport(item, call) -> None:
    if call.when == "call":
        if call.excinfo is not None and "page" in item.funcargs:
            page: Page = item.funcargs["page"]

            allure.attach(
                page.screenshot(type='png'),
                name=f"{slugify(item.nodeid)}.png",
                attachment_type=allure.attachment_type.PNG
            )

            video_path = page.video.path()
            page.context.close()
            allure.attach(
                open(video_path, 'rb').read(),
                name=f"{slugify(item.nodeid)}.webm",
                attachment_type=allure.attachment_type.WEBM
            )


@pytest.fixture(scope="session")
def browser_context_args(tmpdir_factory: pytest.TempdirFactory):
    return {
        "record_video_dir": tmpdir_factory.mktemp('videos'),

        "viewport": {
            "width": 1920,
            "height": 1080,
        }
    }


class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        return requests.post(url=url, params=params, data=data, json=json, headers=headers)

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        return requests.get(url=url, params=params, headers=headers)
